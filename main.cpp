#include <nana/gui.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/slider.hpp>
#include <nana/gui/widgets/checkbox.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <iostream>
using namespace std;
using namespace nana;

int const WINDOW_WIDTH=640;
int const WINDOW_HEIGHT=480;

int main() {

  //CREATES WINDOW
  form window(rectangle(0,0,WINDOW_WIDTH,WINDOW_HEIGHT));

  //CHECKBOX
  checkbox cb(window,rectangle(0,0,200,400),true);
  cb.caption("Checkbox");

  //TEXTBOX
  textbox tb(window,rectangle(0,50,100,50),true);
  tb.caption("I am textbox");

  //DETECTS MOUSE ENTER
  window.events().mouse_enter([]{
    std::cout << "mouse entered" << '\n';
  });

  //DETECTS MOUSE CLICK
  window.events().click([]{
    std::cout << "window clicked" << '\n';
  });
  window.show();
  exec();

  return 0;
}
